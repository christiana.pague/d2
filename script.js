
// External JavaScript
//alert("Hello there!");


/*
Writing Comments:
	- make notes/documentation as they call it
	- comments are ignored by the JS language
	- Two ways to write comments:
	1. single line - denoted by // 
	2. multi-line - denoted by /* vice-versa


	 */
/*
Statement and Syntax

1. Statement = set of instructions, it ends with semicolon ;
2. Syntax = set of rules

*/

/*
Variables and Constants

What is a variable?
	- container (storage) that holds data/value.
	- in variable, value can be change/re-assigned
	- keyword "let"

	syntax: 

		let <variable name> = <value>;

		// variable declaration =		let <variable name>
		// variable initialization =	assigning value to the variable
*/
	
	// var vs. let  - both same keyword to declare variables

	// let cellphone = "iPhone X";

	// use of console

	// console.log(cellphone);


	//______________________________________________

// Is it possible to use single quotes? - Yes

	let greet = 'Hi!';

	console.log(greet);

//What are the scenarios to determine whether to use double & single quotes?
	
	let greetName = "Hi! I'm Joy!"; // double quotes
	console.log(greetName);

	let greetNameV2 = 'Hi! I\'m Joy!'; // single quotes
	console.log(greetNameV2);
		//there are two ways to fix it: Either use double quotes or use escape \
	
	let sheShaid = 'She said, "Hi Joy!"';
	console.log(sheShaid);


	//______________________________________________


	let cellphone = "iPhone X";
	console.log(cellphone);

	// if I am going to change the value, do I need to use let keyword again? - NO need to use let keyword again.
		// We only declare variable once.


	// Can I re-assign a new value to a variable? - Yes

	cellphone = 'Samsung';
	console.log(cellphone);


/*

	What is constant?
		- it is a type of variable bec it holds data/value but its behavior is different from a normal variable.
		- keyword const
		- value is fixed, never change.

*/

	// camel case notation - use to naming variables
	const boilingPointOfTheWater = 100;
	console.log(boilingPointOfTheWater);


	// boilingPointOfTheWater = 32;
	// console.log(boilingPointOfTheWater);


/* Data Types
	- sorting out values that are assigned to our variables

	1. String - alpha-numuric characters denoted by quotes "" & ''

	2. Number - integer/number

	3. Boolean - true or false

	4. Null - declared a variable but the value is null/empty;

	5. Undefined - declared the variable but not sure if there's value or none

	**not defined** - never been declared

	______________________________________________

	
	6. Object - holds multiple data
			- key - value pair
			- denoted by curly braces {}, more specific, separated by comma

		let myGrades = {

			English: 90,
			Math: 92,
			Science: 94,
			Programming: 96
		}

	Array - stores multiple "related" value/data
			- denoted by brackets [] and separated by comma between elements

		let grades = [90, 92, 94, 96];
		
		let fruits = ['apple', 'banana', 'mango'];
*/


console.log('juanDelaCruz2021@mail.com');  // string

console.log(2021);  // number

console.log(true);	// boolean
console.log(false);	// boolean

let lovelife = null; //null
console.log(lovelife);

let me;
console.log(me);

// console.log(sample);

/* Primitive  and Reference Type

*/

	let x = 4;
	let y = x;
	console.log(y); // assigning the value of x to variable y

	x = 8;			// re-assigning new value to variable x that was initially declared and assigned to a value of 4

	// do you think the value of why will change base on the new value assigned to x?
		// checking if y would reference  to x given that it was re-assigned to a new value.
	console.log(y);
				// -answer is No. Primitive value holds a single data and does not reference to other primitive variables.


	let fruits = ['apple', 'banana', 'mango'];
	console.log(fruits);

	let myFruits = fruits
	// reference to array 1

	console.log(myFruits);

	fruits.push('strawberry');

	console.log(fruits);
	console.log(myFruits);


/* Functions

	What is a function?
		- reusable code to prevent duplication



	// display 10 'Hello World! in the console'

	console.log('Hello World!');
	console.log('Hello World!');
	console.log('Hello World!');
	console.log('Hello World!');
	console.log('Hello World!');
	console.log('Hello World!');
	console.log('Hello World!');
	console.log('Hello World!');
	console.log('Hello World!');
	console.log('Hello World!');

	Syntax: 

	function <function name> () {

		statement
	}

*/	//function declaration
	//function invocation



	//function keyword
				//function name
	function displayGreeting() {

			// statement
		console.log('Hello Joy!');
		console.log('Hello Joy!');
		console.log('Hello Joy!');
		console.log('Hello Joy!');
		console.log('Hello Joy!');
		console.log('Hello Joy!');
		console.log('Hello Joy!');
		console.log('Hello Joy!');
		console.log('Hello Joy!');
		console.log('Hello Joy!');

	}

	//calling the function name to execute the function
	//displayGreeting();
	//displayGreeting();


	// is fixed output (hard-coded output) useful all the time?
		// to avoid recurring fixed output, we use:
			// 1. parameter
			// 2. argument

					//parameter
	function greeting(name){

		console.log('Hi' + ' ' + name + '!');
		//console.log(`Hi ${name}!`)

	}
			//argument
	greeting('Sir Vil');
	greeting('Sir Ariel');
	greeting('Ma\'am Rica');


/* Mini-activity

	display 'Hi <firstname> + <lastname>' in the console using the function, and invoke the function 3x by supplying different arguments.

*/

	function sayHi(fName, lName){

		console.log('Hi' + ' ' + fName + ' ' + lName + '!')
		console.log(`Hi ${fName} ${lName}!`)
	}

	sayHi('Sir Mark', 'Vivar');
	sayHi('Sir Roy', 'Ragragio');
	sayHi('Sir Ian', 'Villarta');


/* Mini-activity
	
	create a function that will accept 3 numbers and display the total sum in the console.

*/

	function totalSum(num1, num2, num3){

		console.log(num1 + num2 + num3)

	}

	totalSum(1, 5, 8);
	totalSum(2, 7, 11);



// using return keyword


function sumOfTwoNum(num1, num2){

	console.log('This is the answer:');
	return num1 + num2;

}

console.log(  sumOfTwoNum(8, 7)   );



/***** DO NOT open this topic
var food = 'cake';
console.log(food);

let drink = 'coke';
console.log(drink);

// Scope - means accessibility
	// global scope 
	// function scope



function tryThis(){


	let laptop = 'macbook';
	console.log(laptop);
}

tryThis();

	// console.log(laptop);


var food = 'spag';
console.log(food);

// let drink = 'juice';
// console.log(drink);

*/